package com.example.crm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "customers")
public class Customer {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 100)
    private String lastname;

    @Column(length = 100)
    private String firstname;

    @Column(length = 200)
    private String company;

    @Column(length = 255)
    private String mail;

    @Column(length = 15)
    private String phone;

    @Column(length = 15)
    private String mobile;

    @Column(columnDefinition = "")
    private String notes;

    private Boolean active;

    @OneToMany(mappedBy = "customer")
    private Set<Order> orders;

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", lastname='" + lastname + '\'' +
                ", firstname='" + firstname + '\'' +
                ", company='" + company + '\'' +
                ", mail='" + mail + '\'' +
                ", phone='" + phone + '\'' +
                ", mobile='" + mobile + '\'' +
                ", notes='" + notes + '\'' +
                ", active=" + active +
                ", orders=" + orders +
                '}';
    }
}
