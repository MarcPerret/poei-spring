package com.example.crm.mapper;

import com.example.crm.api.dto.OrderDto;
import com.example.crm.model.Order;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public interface OrderMapper {

    @Mapping(source = "customer.id", target = "customerId")
    @Mapping(source = "customer.mobile", target = "customerPhoneNumber")
    OrderDto mapToDto(Order order);

    @Mapping(source = "customerId", target = "customer.id")
    @Mapping(source = "customerPhoneNumber", target = "customer.mobile")
    Order mapToModel(OrderDto orderDto);
}
