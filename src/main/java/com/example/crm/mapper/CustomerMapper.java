package com.example.crm.mapper;

import com.example.crm.api.dto.CustomerDto;
import com.example.crm.api.dto.OrderDto;
import com.example.crm.model.Customer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface CustomerMapper {

    @Mapping(source = "mobile", target = "numeroTelephone")
    @Mapping(target = "orders", expression = "java(getOrders(customer))")
    CustomerDto mapCustomerToCustomerDto(Customer customer);

    default List<OrderDto> getOrders(Customer customer) {
        if (customer.getOrders() != null) {
            return customer.getOrders().stream()
                    .map(order -> new OrderDto(
                            order.getId(),
                            order.getLabel(),
                            order.getAdrEt(),
                            order.getNumberOfDays(),
                            order.getTva(),
                            order.getStatus(),
                            order.getType(),
                            order.getNotes(),
                            order.getCustomer().getMobile(),
                            customer.getId()
                    ))
                    .toList();
        }
        return new ArrayList<>();
    }

    @Mapping(source = "numeroTelephone", target = "mobile")
    Customer mapCustomerDtoToCustomer(CustomerDto customerDto);
}
