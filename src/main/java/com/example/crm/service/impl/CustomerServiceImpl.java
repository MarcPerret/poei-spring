package com.example.crm.service.impl;

import com.example.crm.exception.NotAllowedToDeleteCustomerException;
import com.example.crm.exception.UnknownResourceException;
import com.example.crm.model.Customer;
import com.example.crm.repository.CustomerRepository;
import com.example.crm.service.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {

    Logger log = LoggerFactory.getLogger(CustomerServiceImpl.class);

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public List<Customer> getAll() {
        return this.customerRepository.findAll(Sort.by("lastname").ascending());
    }

    @Override
    public Customer getById(Integer id) {
        return customerRepository
                .findById(id)
                .orElseThrow(() -> new UnknownResourceException("No customer found for the given ID"));
    }

    @Override
    public Customer createCustomer(Customer customer) {
        log.debug("Attempting to save in DB...");
        return this.customerRepository.save(customer);
    }

    @Override
    public void deleteCustomer(Integer id) {
        Customer customerToDelete = this.getById(id);

        if (this.canDeleteCustomer(customerToDelete)) {
            this.customerRepository.delete(customerToDelete);
        } else {
            throw new NotAllowedToDeleteCustomerException("The given customer still has orders.");
        }
    }

    @Override
    public Customer updateCustomer(Customer customer) {
        log.debug("Attempting to update customer {}", customer.getId());
        Customer existingCustomer = this.getById(customer.getId());
        existingCustomer.setLastname(customer.getLastname());
        existingCustomer.setFirstname(customer.getFirstname());
        existingCustomer.setCompany(customer.getCompany());
        existingCustomer.setActive(customer.getActive());
        return this.customerRepository.save(existingCustomer);
    }

    private boolean canDeleteCustomer(Customer customer) {
        return (null == customer.getOrders() || customer.getOrders().isEmpty());
    }


}
