package com.example.crm.service.impl;

import com.example.crm.exception.InvalidOrderStatusException;
import com.example.crm.exception.NotAllowedToDeleteOrderException;
import com.example.crm.exception.UnknownResourceException;
import com.example.crm.model.Customer;
import com.example.crm.model.Order;
import com.example.crm.repository.CustomerRepository;
import com.example.crm.repository.OrderRepository;
import com.example.crm.service.CustomerService;
import com.example.crm.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    Logger log = LoggerFactory.getLogger(OrderServiceImpl.class);

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CustomerService customerService;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Order> getAll() {
        return orderRepository.findAll(Sort.by("label").ascending());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Order getById(Integer id) {
        return orderRepository.findById(id)
                .orElseThrow(() -> new UnknownResourceException("No order found for the given ID."));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Order createOrder(Order order) {
        log.debug("attempting to create order...");
        return orderRepository.save(order);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteOrder(Integer id) {
        Order order = this.getById(id);
        log.debug("attempting to delete order [{}]", id);
        if (allowedToDeleteOrder(order)) {
            orderRepository.delete(order);
        } else {
            throw new NotAllowedToDeleteOrderException();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Order updateOrder(Order order) {
        log.debug("attempting to update order {}...", order.getId());
        Order existingOrder = this.getById(order.getId());
        existingOrder.setLabel(order.getLabel());
        existingOrder.setNumberOfDays(order.getNumberOfDays());
        existingOrder.setNotes(order.getNotes());
        existingOrder.setStatus(order.getStatus());
        existingOrder.setTva(order.getTva());
        existingOrder.setAdrEt(order.getAdrEt());
        existingOrder.setType(order.getType());
        Customer customer = this.customerService.getById(order.getCustomer().getId());
        existingOrder.setCustomer(customer);
        return orderRepository.save(existingOrder);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Order patchOrderStatus(Integer orderId, String status) {
        log.debug("attempting to patch order {} with status {}...", orderId, status);
        if (status != null && status.length() < 31) {
            Order existingOrder = this.getById(orderId);
            existingOrder.setStatus(status);
            return orderRepository.save(existingOrder);
        } else {
            throw new InvalidOrderStatusException();
        }

    }

    /**
     * Determine if an order can be deleted (status ?)
     *
     * @param order the order
     * @return true if the order can be deleted, false otherwise
     */
    private boolean allowedToDeleteOrder(Order order) {
        return !(null != order.getStatus() && !"En cours".equals(order.getStatus()));
    }

}
