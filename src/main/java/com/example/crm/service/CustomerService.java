package com.example.crm.service;

import com.example.crm.model.Customer;

import java.util.List;

public interface CustomerService {

    List<Customer> getAll();

    Customer getById(Integer id);

    /**
     * Create a customer
     * @param customer
     * @return
     */
    Customer createCustomer(Customer customer);

    void deleteCustomer(Integer id);

    Customer updateCustomer(Customer customer);

}
