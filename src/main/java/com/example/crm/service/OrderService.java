package com.example.crm.service;


import com.example.crm.model.Order;

import java.util.List;

public interface OrderService {
	
	/**
	 * Get all orders
	 * @return
	 */
	List<Order> getAll();
	
	/**
	 * Get order by id
	 * @param id the id
	 * @return
	 */
	Order getById(Integer id);
	
	/**
	 * Create order
	 * @param order the order to create
	 * @return the created order
	 */
	Order createOrder(Order order);
	
	/**
	 * Delete order
	 * @param id the order id
	 */
	void deleteOrder(Integer id);
	
	/**
	 * Update order
	 * @param order the order
	 * @return the updated order
	 */
	Order updateOrder(Order order);
	
	/**
	 * Update order status
	 * @param orderId the order Id
	 * @param status the new status
	 */
	Order patchOrderStatus(Integer orderId, String status);

}
