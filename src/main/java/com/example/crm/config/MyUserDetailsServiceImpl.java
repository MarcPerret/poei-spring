package com.example.crm.config;

import com.example.crm.exception.UnknownResourceException;
import com.example.crm.model.User;
import com.example.crm.repository.UserRepository;
import com.example.crm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class MyUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        try {
            User existingUser = this.userService.getByUsername(username);
            return new MyUserDetails(existingUser);
        } catch (UnknownResourceException ure) {
            throw new UsernameNotFoundException("User with username " + username + " not found.");
        }
    }
}
