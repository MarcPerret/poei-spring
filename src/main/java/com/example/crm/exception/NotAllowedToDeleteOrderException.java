package com.example.crm.exception;

public class NotAllowedToDeleteOrderException extends RuntimeException {

    public NotAllowedToDeleteOrderException(String message) {
        super(message);
    }

     public NotAllowedToDeleteOrderException() {
        super("Cannot delete the given order.");
     }
}
