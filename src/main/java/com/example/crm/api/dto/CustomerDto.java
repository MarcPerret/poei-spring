package com.example.crm.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CustomerDto {

    private Integer id;
    private String firstname;
    private String lastname;
    private String numeroTelephone;
    private String company;
    private String mail;
    private String notes;
    private Boolean active;
    private List<OrderDto> orders;

}
