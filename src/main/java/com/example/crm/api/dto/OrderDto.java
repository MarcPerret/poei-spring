package com.example.crm.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderDto {

    private Integer id;
    private String label;
    private Double adrEt;
    private Double numberOfDays;
    private Double tva;
    private String status;
    private String type;
    private String notes;
    private String customerPhoneNumber;
    private Integer customerId;
}
