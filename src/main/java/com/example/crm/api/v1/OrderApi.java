package com.example.crm.api.v1;

import com.example.crm.api.dto.OrderDto;
import com.example.crm.exception.InvalidOrderStatusException;
import com.example.crm.exception.NotAllowedToDeleteOrderException;
import com.example.crm.exception.UnknownResourceException;
import com.example.crm.mapper.OrderMapper;
import com.example.crm.service.OrderService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/v1/orders")
@CrossOrigin(value = {"*"}, allowedHeaders = {"*"})
public class OrderApi {

    public static final String ORDER_NOT_FOUND = "Order Not Found";

    Logger log = LoggerFactory.getLogger(OrderApi.class);

    private OrderService orderService;

    private OrderMapper orderMapper;

    public OrderApi(OrderService orderService, OrderMapper orderMapper) {
        this.orderService = orderService;
        this.orderMapper = orderMapper;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Return the list of all orders", responses = @ApiResponse(responseCode = "200", content = @Content(array = @ArraySchema(schema = @Schema(implementation = OrderDto.class)))))
    public ResponseEntity<List<OrderDto>> getAll() {
        return ResponseEntity
                .ok(orderService.getAll().stream()
                        .map(order -> this.orderMapper.mapToDto(order))
                        .collect(Collectors.toList()));
    }

    @GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Return an order", responses = @ApiResponse(responseCode = "200", content = @Content(array = @ArraySchema(schema = @Schema(implementation = OrderDto.class)))))
    public ResponseEntity<OrderDto> getById(@PathVariable final Integer id) {
        try {
            return ResponseEntity.ok(orderMapper.mapToDto(orderService.getById(id)));
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ORDER_NOT_FOUND);
        }
    }

    @PostMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE,
            MediaType.TEXT_PLAIN_VALUE})
    @Operation(summary = "Create order", responses = @ApiResponse(responseCode = "201", description = "Created"))
    public ResponseEntity<OrderDto> createOrder(@RequestBody final OrderDto orderDto) {
        log.debug("Attempting to create order with label {}", orderDto.getLabel());
        OrderDto newOrder = orderMapper
                .mapToDto(orderService
                        .createOrder(orderMapper.mapToModel(orderDto)));
        return ResponseEntity.created(URI.create("/v1/orders/" + newOrder.getId())).body(newOrder);
    }

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "Delete order", responses = @ApiResponse(responseCode = "204", description = "No Content"))
    public ResponseEntity<Void> deleteOrder(@PathVariable final Integer id) {
        try {
            log.debug("Preparing to delete order with id [{}]", id);
            orderService.deleteOrder(id);
            return ResponseEntity.noContent().build();
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ORDER_NOT_FOUND);
        } catch (NotAllowedToDeleteOrderException ex) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, ex.getMessage());
        }
    }

    @PutMapping(path = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {
            MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE})
    @Operation(summary = "Update order", responses = @ApiResponse(responseCode = "204", description = "No Content"))
    public ResponseEntity<Void> updateOrder(@PathVariable final Integer id, @RequestBody OrderDto orderDto) {
        try {
            log.debug("Updating order {}", id);
            orderDto.setId(id);
            orderService.updateOrder(orderMapper.mapToModel(orderDto));
            log.debug("Successfully updated order {}", id);
            return ResponseEntity.noContent().build();
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ORDER_NOT_FOUND);
        }
    }

    @PatchMapping("/{id}")
    @Operation(summary = "Patch order's status for the given ID and order")
    public ResponseEntity<OrderDto> patchOrderStatus(@PathVariable final Integer id, @RequestBody OrderDto orderDto) {
        try {
            log.debug("Patching order {}", id);
            orderDto.setId(id);
            return ResponseEntity.ok(orderMapper.mapToDto(orderService.patchOrderStatus(id, orderDto.getStatus())));
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        } catch (InvalidOrderStatusException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, ex.getMessage());
        }
    }

}
