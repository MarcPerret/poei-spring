package com.example.crm.api.v1;

import com.example.crm.api.dto.UserDto;
import com.example.crm.exception.UnknownResourceException;
import com.example.crm.mapper.UserMapper;
import com.example.crm.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/v1/users")
public class UserApi {

    private final UserService userService;
    private final UserMapper userMapper;

    public UserApi(UserService userService, UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Return the list of all users")
    public ResponseEntity<List<UserDto>> getAll() {
        return ResponseEntity.ok(
                userService.getAll().stream()
                        .map(userMapper::mapToDto)
                        .toList()
        );
    }

    @GetMapping(path = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Return a user by its id")
    public ResponseEntity<UserDto> getUserById(@PathVariable Integer id) {
        try {
            return ResponseEntity.ok(userMapper.mapToDto(userService.getById(id)));
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }
    }

    @PostMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Create a user")
    public ResponseEntity<UserDto> createUser(@RequestBody UserDto userDto) {
        UserDto userCreated = userMapper.mapToDto(userService.createUser(userMapper.mapToModel(userDto)));
        return ResponseEntity.created(URI.create("/v1/users/" + userCreated.getId()))
                .body(userCreated);
    }

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "Delete a user")
    public ResponseEntity<Void> deleteUser(@PathVariable final Integer id) {
        try {
            userService.deleteUser(id);
            return ResponseEntity.noContent().build();
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }
    }

    @PutMapping(path = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Update user")
    public ResponseEntity<UserDto> updateUSer(@RequestBody UserDto userDto, @PathVariable final Integer id) {
        try {
            userDto.setId(id);
            UserDto updatedUser = userMapper.mapToDto(userService.update(userMapper.mapToModel(userDto)));
            return ResponseEntity.ok(updatedUser);
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }
    }

    @GetMapping(path = "/login", produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Return a user by its username and password")
    public ResponseEntity<UserDto> login(@RequestParam String username, @RequestParam String password) {
        try {
            return ResponseEntity.ok(
                    userMapper.mapToDto(userService.getUserByUsernameAndPassword(username, password))
            );
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, ure.getMessage());
        }
    }

}
